﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindManager : MonoBehaviour {

    public List<Vector3> positions = new List<Vector3>();

    private bool _gameStarted;
    public bool gameStarted
    {
        get {
            return _gameStarted;
        }

        set
        {
            if (_gameStarted != value)
            {
                _gameStarted = value;
                if (_gameStarted)
                {
                    StartGame();
                }
            }
        }
    }
    public float maxRewindTime;
    public float rewindInterval;
    public bool isRewinding;
    public Vector3 currentObjective;
    public float rewindSpeed;
    public int counter;

	// Use this for initialization
	void Start () {
        gameStarted = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (isRewinding)
        {
            Rewind(Time.deltaTime);
        }
	}

    void StartGame()
    {
        positions.Clear();
        InvokeRepeating("AddPosition", 0, rewindInterval);
    }

    void AddPosition()
    {
        positions.Add(transform.position);
        if (positions.Count > (maxRewindTime / rewindInterval))
        {
            positions.RemoveAt(0);
        }
   //     Debug.Log(positions.Count);
    }

    void Rewind(float deltaTime)
    {
        if ((counter >= positions.Count - 1 - (float)positions.Count / 10f) || (counter <= (float)positions.Count / 10f))
        {
            GetComponentInChildren<Renderer>().enabled = true;
        } else
        {
            GetComponentInChildren<Renderer>().enabled = false;
        }
        if (transform.position != currentObjective)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentObjective, rewindSpeed * deltaTime);
        } else
        {
            if (counter > 0)
            {
                counter--;
                currentObjective = positions[counter];
            } else
            {
                isRewinding = false;
                StartGame();
                GetComponent<Collider>().enabled = true;
                GetComponent<PlayerMovement>().enabled = true;
                GetComponent<Rigidbody>().isKinematic = false;
                GetComponent<PlayerInfo>().currentTime = GetComponent<PlayerInfo>().maxTime;
            }
        }
    }

    public void StartRewind()
    {
        isRewinding = true;
        currentObjective = positions[positions.Count - 1];
        counter = positions.Count - 1;
        CancelInvoke();
        GetComponent<Collider>().enabled = false;
        GetComponent<PlayerMovement>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
    }
}
