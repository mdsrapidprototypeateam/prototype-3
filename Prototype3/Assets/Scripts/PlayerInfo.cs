﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

	public int team;
	public int currentTime;
	public int maxTime;
    public float currentAmmo;
    public float maxAmmo;

    [FMODUnity.EventRef]
    public string hit = "event:/Hit";       //Create the eventref and define the event path
    public string rewindSound = "event:/Rewind";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance hitEvent;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentAmmo = GetComponent<PlayerMovement>().currentWeapon.currentAmmo;
        maxAmmo = GetComponent<PlayerMovement>().currentWeapon.maxAmmo;
    }

    public void DealDamage(int time)
    {
        currentTime -= time;
        FMODUnity.RuntimeManager.PlayOneShot(hit);
        if (currentTime <= 0)
        {
            GetComponent<RewindManager>().StartRewind();
            FMODUnity.RuntimeManager.PlayOneShot(rewindSound);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TimeArea")
        {
            other.GetComponent<AreaCircle>().list.Add(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "TimeArea")
        {
            other.GetComponent<AreaCircle>().list.Remove(this);
        }
    }
}
