﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EPlayerId { PLAYER1, PLAYER2, PLAYER3, PLAYER4, NONE };
public enum EPlayerControls { KEYBOARD_MOUSE, JOYSTICK, NONE };
public enum EPlayerState
{
    NONE,
    STATE_IDLE,
    STATE_FOUND,
    STATE_GAME_INACTIVE,
    STATE_READY
}

public class PlayerId : MonoBehaviour
{
    private MyGameManager gameManager;
    public EPlayerId playerId = EPlayerId.PLAYER1;

    public EPlayerState playerState = EPlayerState.NONE;
    private EPlayerState mPlayerState = EPlayerState.NONE;

    public delegate void EPlayerStateChanged(EPlayerState value);
    public event EPlayerStateChanged OnEPlayerStateChangedEvent;


    public EPlayerControls playerControls = EPlayerControls.NONE;
    private EPlayerControls mPlayerControls = EPlayerControls.NONE;

    public delegate void EPlayerControlsChanged(EPlayerControls value);
    public event EPlayerControlsChanged OnEPlayerControlsChangedEvent;

    public PlayerMovement movementInputComponent;
    public PlayerMenuNavigation menuInputComponent;

    private MeshRenderer childModelMeshRenderer;

    void Start()
    {
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        movementInputComponent = GetComponent<PlayerMovement>();
        menuInputComponent = GetComponent<PlayerMenuNavigation>();

        StartEPlayerStateCounter();
        StartEPlayerControlsCounter();
        
        childModelMeshRenderer = transform.Find("CharacterModel").GetComponent<MeshRenderer>();
        if (playerId == EPlayerId.PLAYER2 || playerId == EPlayerId.PLAYER4 )
        {
            childModelMeshRenderer.enabled = true;
        }
        else childModelMeshRenderer.enabled = false;
    }


    void Update()
    {
        //listen for changes on player state
        GetEPlayerState();
        GetEPlayerControls();

        if (playerState == EPlayerState.NONE)
        {
            playerState = EPlayerState.STATE_IDLE;
        }

        if ( playerControls == EPlayerControls.NONE )
        {
            if ( playerId != EPlayerId.PLAYER1 )
                playerControls = EPlayerControls.JOYSTICK;
            else
                playerControls = EPlayerControls.KEYBOARD_MOUSE;
        }
    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEPlayerStateCounter()
    {
        OnEPlayerStateChangedEvent += OnEPlayerStateChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEPlayerState()
    {
        if ( playerState != mPlayerState )
        {
            mPlayerState = playerState;

            if ( OnEPlayerStateChangedEvent != null )
                OnEPlayerStateChangedEvent(mPlayerState);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEPlayerStateChanged(EPlayerState value)
    {
        // state effects
        switch (value)
        {
            case EPlayerState.STATE_FOUND:
                ++gameManager.playersFoundCount;
                gameManager.ListenForPlayers(playerId);
                gameManager.RevealPlayer(playerId);

                playerState = EPlayerState.STATE_READY;
                break;

            case EPlayerState.STATE_READY:
                ++gameManager.playersReadyCount;
                gameManager.ListenForPlayers(playerId);
                gameManager.MakePlayerReady(playerId);
                break;
        }
        
    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEPlayerControlsCounter()
    {
        OnEPlayerControlsChangedEvent += OnEPlayerControlsChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEPlayerControls()
    {
        if ( playerControls != mPlayerControls )
        {
            mPlayerControls = playerControls;

            if ( OnEPlayerControlsChangedEvent != null )
                OnEPlayerControlsChangedEvent(mPlayerControls);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEPlayerControlsChanged(EPlayerControls value)
    {
        // state effects
        if ( value == EPlayerControls.KEYBOARD_MOUSE )
            LoadKeyboarMouseInputMap();
        else if ( value == EPlayerControls.JOYSTICK )
            LoadJoystickInputMap();
    }

    /// <summary>
    /// Load keyboard and mouse input map
    /// </summary>
    private void LoadKeyboarMouseInputMap()
    {
        if ( movementInputComponent )
        {
            movementInputComponent.horizontalMovAxisName = "MovHorizontal";
            movementInputComponent.verticalMovAxisName = "MovVertical";
            movementInputComponent.horizontalAimAxisName = "AimHorizontal";
            movementInputComponent.verticalAimAxisName = "AimVertical";
            movementInputComponent.fireButtonName = "Fire1";
            movementInputComponent.jumpButtonName = "Jump";
            movementInputComponent.startButtonName = "Submit";
            movementInputComponent.reloadButtonName = "Reload";

            movementInputComponent.AddSufixToInputNames();
        }
        if ( menuInputComponent )
        {
            menuInputComponent.horizontalMovAxisName = "MovHorizontal";
            menuInputComponent.verticalMovAxisName = "MovVertical";
            menuInputComponent.horizontalAimAxisName = "AimHorizontal";
            menuInputComponent.verticalAimAxisName = "AimVertical";
            menuInputComponent.fireButtonName = "Fire1";
            menuInputComponent.jumpButtonName = "Jump";
            menuInputComponent.startButtonName = "Submit";
            menuInputComponent.reloadButtonName = "Reload";

            menuInputComponent.AddSufixToInputNames();
        }
    }

    /// <summary>
    /// Load joystick input map
    /// </summary>
    private void LoadJoystickInputMap()
    {
        if ( movementInputComponent )
        {
            movementInputComponent.horizontalMovAxisName = "L_XAxis";
            movementInputComponent.verticalMovAxisName = "L_YAxis";
            movementInputComponent.horizontalAimAxisName = "R_XAxis";
            movementInputComponent.verticalAimAxisName = "R_YAxis";
            movementInputComponent.fireButtonName = "TriggersR";
            movementInputComponent.jumpButtonName = "A";
            movementInputComponent.startButtonName = "Start";
            movementInputComponent.reloadButtonName = "X";

            movementInputComponent.AddSufixToInputNames();
        }
        if ( menuInputComponent )
        {
            menuInputComponent.horizontalMovAxisName = "L_XAxis";
            menuInputComponent.verticalMovAxisName = "L_YAxis";
            menuInputComponent.horizontalAimAxisName = "R_XAxis";
            menuInputComponent.verticalAimAxisName = "R_YAxis";
            menuInputComponent.fireButtonName = "TriggersR";
            menuInputComponent.jumpButtonName = "A";
            menuInputComponent.startButtonName = "Start";
            menuInputComponent.reloadButtonName = "X";

            menuInputComponent.AddSufixToInputNames();
        }
    }
}
