﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PlayerInputBase
{
    Rigidbody rb;
    RewindManager rw;
    Transform cam;

    public Weapon currentWeapon;

    public GameObject projectilePrefab;
    bool canShoot = true;

    bool movementSoundIsPlaying;

    [FMODUnity.EventRef]
    public string movingSound = "event:/Walking";
    FMOD.Studio.EventInstance movingEvent;

    [Space(6f)]
    [Header("Movement")]
    public float yRotation = 0f;
    public float yRotationSpeed = 10f;
    public float vRotationSpeed = 10f;
    public float characterSpeed = 10f;
    public float jumpPower = 10f;

    public override void Start ()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        rw = GetComponent<RewindManager>();
        cam = GetComponentInChildren<Camera>().transform;
        Cursor.lockState = CursorLockMode.Locked;
        movingEvent = FMODUnity.RuntimeManager.CreateInstance(movingSound);
    }

    public override void Update()
    {
        GetPlayerInput(Time.deltaTime);
        if (m_bIsFireHold)
        {
            currentWeapon.Fire(cam);
        }
    }

    public override void FixedUpdate ()
    {
        GetPlayerPhysicsInput(Time.fixedDeltaTime);
	}

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public override void GetPlayerPhysicsInput(float deltaTime)
    {
        if ( gameManager.e_State == EGameState.STATE_GAME_STARTED )
        {
            m_fMovHorizontalInput = Input.GetAxis(horizontalMovAxisName);
            float horizontalMov = characterSpeed * deltaTime * m_fMovHorizontalInput;
            /*if ( m_fMovHorizontalInput > 0 )
            {
                cc.Move(new Vector3(characterSpeed, gravity, 0) * deltaTime * m_fMovHorizontalInput);
            }
            else if ( m_fMovHorizontalInput < 0 )
            {
                cc.Move(new Vector3(characterSpeed, 0, 0) * deltaTime * m_fMovHorizontalInput);
            }*/
            m_fMovVerticalInput = Input.GetAxis(verticalMovAxisName);
            float verticalMov = characterSpeed * deltaTime * m_fMovVerticalInput;
            /*
            if ( m_fMovVerticalInput > 0 )
            {
                cc.Move(new Vector3(0, 0, characterSpeed) * deltaTime * m_fMovVerticalInput);
            }
            else if ( m_fMovVerticalInput < 0 )
            {
                cc.Move(new Vector3(0, 0, characterSpeed) * deltaTime * m_fMovVerticalInput);
            }*/
            rb.MovePosition(rb.position + transform.rotation * new Vector3(horizontalMov, 0, verticalMov));

            m_FireTriggerInput = Input.GetAxis(fireButtonName);

            if (!movementSoundIsPlaying)
            {
                movingEvent.start();
                movementSoundIsPlaying = true;
            }

            if (m_fMovHorizontalInput == 0 && m_fMovVerticalInput == 0)
            {
                movingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                movementSoundIsPlaying = false;
                
            }
        }
    }

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public override void GetPlayerInput(float deltaTime)
    {
        if (gameManager.e_State == EGameState.STATE_GAME_STARTED)
        {
            m_fAimHorizontalInput = Input.GetAxis(horizontalAimAxisName);
            if ( m_fAimHorizontalInput > 0 )
            {
                
            }
            else if ( m_fMovHorizontalInput < 0 )
            {
                
            }
            m_fAimVerticalInput = Input.GetAxis(verticalAimAxisName);
            if ( m_fAimVerticalInput > 0 )
            {
                
            }
            else if ( m_fMovVerticalInput < 0 )
            {
                
            }

            transform.Rotate(new Vector3(0, m_fAimHorizontalInput * deltaTime * yRotationSpeed));

            float verticalRot = m_fAimVerticalInput * deltaTime * -vRotationSpeed;
            

            if (verticalRot + cam.localRotation.eulerAngles.x > 280 || verticalRot + cam.localRotation.eulerAngles.x < 45)
            {
                if (cam.localRotation.eulerAngles.x > 280 || cam.localRotation.eulerAngles.x < 45)
                {
                    cam.Rotate(new Vector3(verticalRot, 0));
                }
            }

            if (Input.GetAxisRaw(fireButtonName) != 0.0f && !m_bIsFireHold)
            {
                m_bIsFireHold = true;
            }

            else if (Input.GetAxisRaw(fireButtonName) == 0.0f && m_bIsFireHold)
            {
                m_bIsFireHold = false;
                currentWeapon.StopFire();
            }

            if ( Input.GetButtonDown(jumpButtonName) && !m_bIsJumpHold)
            {
                m_bIsJumpHold = true;
                RaycastHit hit;
                if (Physics.Raycast(transform.position, -transform.up, out hit, 1.5f))
                {
                    rb.AddForce(new Vector3(0, jumpPower, 0), ForceMode.Impulse);
                }
                //rw.StartRewind();
            }
            else if ( Input.GetButtonUp(jumpButtonName) && m_bIsJumpHold )
                m_bIsJumpHold = false;

            if ( Input.GetButtonDown(startButtonName) && !m_bIsStartHold )
                m_bIsStartHold = true;
            else if ( Input.GetButtonUp(startButtonName) && m_bIsStartHold )
                m_bIsStartHold = false;

            if (Input.GetButtonDown(reloadButtonName) && !m_bIsReloadHold)
            {
                m_bIsReloadHold = true;
                if (currentWeapon.currentAmmo < currentWeapon.maxAmmo)
                {
                    currentWeapon.Reload();
                }
            }

            else if (Input.GetButtonUp(reloadButtonName) && m_bIsReloadHold)
            {
                m_bIsReloadHold = false;
            }
        }
    }

   

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TimeArea")
        {
            cam.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "TimeArea")
        {
            cam.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().enabled = false;
        }
    }
}
