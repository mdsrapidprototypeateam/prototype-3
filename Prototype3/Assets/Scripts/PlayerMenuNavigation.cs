﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMenuNavigation : PlayerInputBase
{
    //public float allowedIdleTimeAfterFindingPlayer = 60f;

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        GetPlayerInput(Time.deltaTime);
    }

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public override void GetPlayerInput(float deltaTime)
    {
        if ( (gameManager.e_State == EGameState.STATE_IDLE || gameManager.e_State == EGameState.STATE_MENU) && playerIdComponent.playerState == EPlayerState.STATE_IDLE )
        {
            if ( Input.GetButtonDown(fireButtonName) && !m_bIsFireHold )
            {
                m_bIsFireHold = true;
            }
            else if ( Input.GetButtonUp(fireButtonName) && m_bIsFireHold )
            {
                m_bIsFireHold = false;
            }

            if ( Input.GetButtonDown(jumpButtonName) && !m_bIsJumpHold )
            {
                m_bIsJumpHold = true;
            }
            else if ( Input.GetButtonUp(jumpButtonName) && m_bIsJumpHold )
            {
                m_bIsJumpHold = false;
            }

            //start pressing START button
            if ( Input.GetButtonDown(startButtonName) && !m_bIsStartHold )
            {
                m_bIsStartHold = true;
            }
            //released START button
            else if ( Input.GetButtonUp(startButtonName) && m_bIsStartHold )
            {
                ChangePlayerStateToStateFound();
                //Invoke("ChangePlayerStateToStateIdleFromInactivity", allowedIdleTimeAfterFindingPlayer);
                m_bIsStartHold = false;
            }
        }

        else if ( gameManager.e_State == EGameState.STATE_MENU && playerIdComponent.playerState == EPlayerState.STATE_FOUND)
        {
            if ( Input.GetButtonDown(fireButtonName) && !m_bIsFireHold )
            {
                m_bIsFireHold = true;
            }
            else if ( Input.GetButtonUp(fireButtonName) && m_bIsFireHold )
            {
                m_bIsFireHold = false;
            }

            if ( Input.GetButtonDown(jumpButtonName) && !m_bIsJumpHold )
            {
                m_bIsJumpHold = true;
            }
            else if ( Input.GetButtonUp(jumpButtonName) && m_bIsJumpHold )
            {
                m_bIsJumpHold = false;
            }

            //start pressing START button
            if ( Input.GetButtonDown(startButtonName) && !m_bIsStartHold )
            {
                m_bIsStartHold = true;
            }
            //released START button
            else if ( Input.GetButtonUp(startButtonName) && m_bIsStartHold )
            {
                ChangePlayerStateToStateReady();
                m_bIsStartHold = false;
            }
        }

        else if ( gameManager.e_State == EGameState.STATE_FINISH && playerIdComponent.playerState == EPlayerState.STATE_GAME_INACTIVE )
        {
            //start pressing START button
            if ( Input.GetButtonDown(startButtonName) && !m_bIsStartHold )
            {
                m_bIsStartHold = true;
            }
            //released START button
            else if ( Input.GetButtonUp(startButtonName) && m_bIsStartHold )
            {
                ChangePlayerStateToStateReady();
                //gameManager.e_State = EGameState.STATE_MENU;
                m_bIsStartHold = false;
            }
        }
    }

    void ChangePlayerStateToStateFound()
    {
        playerIdComponent.playerState = EPlayerState.STATE_FOUND;
    }

    void ChangePlayerStateToStateReady()
    {
        playerIdComponent.playerState = EPlayerState.STATE_READY;
    }

    void ChangePlayerStateToStateIdle()
    {
        playerIdComponent.playerState = EPlayerState.STATE_IDLE;
    }

    void ChangePlayerStateToStateIdleFromInactivity()
    {
        if ( playerIdComponent.playerState == EPlayerState.STATE_FOUND )
            ChangePlayerStateToStateIdle();
    }
}
