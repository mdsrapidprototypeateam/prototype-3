﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EGameState
{
    NONE,
    STATE_BOOTING,
    STATE_IDLE,
    STATE_MENU,
    STATE_COUNTDOWN,
    STATE_GAME_STARTED,
    STATE_GAME_SCORED,
    STATE_BALL_OUTOFFBOUNDS,
    STATE_FINISH
    //STATE_WIN
}

public enum EVersusMode { ONE_ONE, TWO_TWO };

public class MyGameManager : MonoBehaviour
{
    public EGameState e_State = EGameState.NONE;
    private EGameState m_EState;

    public EVersusMode e_VersusMode = EVersusMode.ONE_ONE;
    private EVersusMode m_EVersusMode = EVersusMode.ONE_ONE;

    public delegate void EGameStateChanged(EGameState value);
    public event EGameStateChanged OnEGameStateChangedEvent;

    public delegate void EVersusModeChanged(EVersusMode value);
    public event EVersusModeChanged OnEVersusModeChangedEvent;

    public GameObject UI_CanvasTitle;
    public GameObject UI_CanvasTitleBackground;
    public GameObject UI_CanvasGameplay;
    public GameObject UI_CanvasFinish;
    public GameObject[] PlayerObjects;
    public GameObject[] PlayerTitleUIObjects;
    public GameObject[] PlayerFinishUIObjects;
    public GameObject[] PlayerUIObjects;
    public Camera[] PlayerCameras;

    public int playersFoundCount = 0;
    public int playersReadyCount = 0;

    public float timeUntilReboot = 5.0f;

    public int winningTeam = 0;

    //public List<EPlayerId> playersFoundOrder;

    public static MyGameManager GetInstance()
    {
        return GameObject.Find("GameManager").GetComponent<MyGameManager>();
    }

    void Start()
    {
        //Start tracking changes to game state
        StartEGameStateCounter();
        GetEGameState();

        //Set initial game state to idle
        if ( e_State.Equals(EGameState.NONE) )
            e_State = EGameState.STATE_BOOTING;

        GameObject uiParent = GameObject.FindWithTag("UI_Parent");
        UI_CanvasTitle = uiParent.transform.Find("Title").gameObject;
        UI_CanvasTitleBackground = uiParent.transform.Find("Title_BG").gameObject;
        UI_CanvasGameplay = uiParent.transform.Find("Game").gameObject;
        UI_CanvasFinish = uiParent.transform.Find("Finish").gameObject;

        GameObject[] tempPlayerObjects = GameObject.FindGameObjectsWithTag("Player");
        int finalSize = tempPlayerObjects.Length;
        PlayerObjects = new GameObject[finalSize];

        PlayerId[] playerIdComponents = GameObject.FindObjectsOfType<PlayerId>();
        int size = playerIdComponents.Length;
        //Debug.Log("Found " + size +"player id components");
        for (int i = 0; i < size; i++ )
        {
            switch ( playerIdComponents[i].playerId )
            {
                case EPlayerId.PLAYER1:
                    PlayerObjects[0] = playerIdComponents[i].gameObject;
                    Debug.Log("Found " + size + "player id components");
                    break;
                case EPlayerId.PLAYER2:
                    PlayerObjects[1] = playerIdComponents[i].gameObject;
                    Debug.Log("Found " + size + "player id components");
                    break;
                case EPlayerId.PLAYER3:
                    PlayerObjects[2] = playerIdComponents[i].gameObject;
                    Debug.Log("Found " + size + "player id components");
                    break;
                case EPlayerId.PLAYER4:
                    PlayerObjects[3] = playerIdComponents[i].gameObject;
                    Debug.Log("Found " + size + "player id components");
                    break;
            }
        }

        PlayerCameras = new Camera[finalSize];
        for ( int i = 0; i < finalSize; i++ )
        {
            PlayerCameras[i] = PlayerObjects[i].transform.GetComponentInChildren<Camera>();
        }

        size = UI_CanvasTitle.transform.Find("Players").childCount;
        PlayerTitleUIObjects = new GameObject[size];
        for ( int i = 0; i < size; i++ )
        {
            PlayerTitleUIObjects[i] = UI_CanvasTitle.transform.Find("Players").GetChild(i).gameObject;
            PlayerTitleUIObjects[i].SetActive(false);
        }

        size = UI_CanvasFinish.transform.Find("Players").childCount;
        PlayerFinishUIObjects = new GameObject[size];
        for ( int i = 0; i < size; i++ )
        {
            PlayerFinishUIObjects[i] = UI_CanvasFinish.transform.Find("Players").GetChild(i).gameObject;
            PlayerFinishUIObjects[i].SetActive(true);
        }
    }


    void Update()
    {
        //Get current game state
        GetEGameState();
    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEGameStateCounter()
    {
        OnEGameStateChangedEvent += OnEGameStateChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEGameState()
    {
        if ( e_State != m_EState )
        {
            m_EState = e_State;

            if ( OnEGameStateChangedEvent != null )
                OnEGameStateChangedEvent(m_EState);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEGameStateChanged(EGameState value)
    {
        switch ( value )
        {
            case EGameState.STATE_BOOTING:
                UI_CanvasTitle.SetActive(false);
                UI_CanvasGameplay.SetActive(false);
                UI_CanvasFinish.SetActive(false);
                Invoke("ChangeToIdleState", 5.0f);
                //Invoke("ChangeToGameStartedState", 5.0f);
                break;

            case EGameState.STATE_IDLE:
                UI_CanvasTitle.SetActive(true);
                break;

            case EGameState.STATE_MENU:
                //
                break;

            case EGameState.STATE_COUNTDOWN:
                //
                break;

            case EGameState.STATE_GAME_STARTED:
                OnGameStarted();
                break;

            case EGameState.STATE_FINISH:
                OnGameFinished();
                break;
        }
    }


    void ChangeToIdleState()
    {
        e_State = EGameState.STATE_IDLE;
    }

    void ChangeToMenuState()
    {
        e_State = EGameState.STATE_MENU;
    }

    void ChangeToGameStartedState()
    {
        e_State = EGameState.STATE_GAME_STARTED;
    }

    void ChangeToGameFinishState()
    {
        e_State = EGameState.STATE_FINISH;
    }

    void OnGameStarted()
    {
        UI_CanvasTitle.SetActive(false);
        UI_CanvasTitleBackground.SetActive(false);
        UI_CanvasGameplay.SetActive(true);
        UI_CanvasFinish.SetActive(false);

        if (e_VersusMode == EVersusMode.ONE_ONE )
        {
            UI_CanvasGameplay.transform.Find("HorizontalDivider").gameObject.SetActive(false);
            PlayerCameras[0].rect = new Rect(0.0f, 0.0f, 0.5f, 1.0f);
            PlayerCameras[1].rect = new Rect(0.5f, 0.0f, 0.5f, 1.0f);

            PlayerUIObjects[2].gameObject.SetActive(false);
            PlayerUIObjects[3].gameObject.SetActive(false);

            PlayerCameras[2].transform.parent.gameObject.SetActive(false);
            PlayerCameras[3].transform.parent.gameObject.SetActive(false);
        }
        else
        {

            UI_CanvasGameplay.transform.Find("HorizontalDivider").gameObject.SetActive(true);
            PlayerCameras[2].transform.parent.gameObject.SetActive(true);
            PlayerCameras[3].transform.parent.gameObject.SetActive(true);
            PlayerUIObjects[2].gameObject.SetActive(true);
            PlayerUIObjects[3].gameObject.SetActive(true);
            PlayerCameras[0].rect = new Rect(0.0f, 0.5f, 0.5f, 0.5f);
            PlayerCameras[1].rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
            PlayerCameras[2].rect = new Rect(0.0f, 0.0f, 0.5f, 0.5f);
            PlayerCameras[3].rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
        }
    }

    public void ListenForPlayers(EPlayerId playerId)
    {
        if (playersFoundCount == 1)
        {
            ChangeToMenuState();
        }

        if (playersFoundCount > 2 || playerId == EPlayerId.PLAYER3 || playerId == EPlayerId.PLAYER4)
        {
            e_VersusMode = EVersusMode.TWO_TWO;
        }

        if ((e_VersusMode == EVersusMode.TWO_TWO && playersReadyCount == 4) ||
            (e_VersusMode == EVersusMode.ONE_ONE && playersReadyCount == 2))
        {
            Invoke("ChangeToGameStartedState", 2.0f);
        }
    }

    public void RevealPlayer(EPlayerId ePlayerId)
    {
        if( playersFoundCount == 1)
        {
            for ( int i = 0; i < PlayerTitleUIObjects.Length; i++ )
            {
                PlayerTitleUIObjects[i].SetActive(true);
            }
        }

        Color newColor = new Color(1f,1f,1f,0.5f);

        Text textComp = UI_CanvasTitle.transform.Find("Text").GetComponent<Text>();
        string text = "";
        if ( playersFoundCount < 3 && playersFoundCount > 0 )
            text = textComp.text;

        //playersFoundOrder.Add(ePlayerId);

        switch ( ePlayerId )
        {
            //.text = "WAITING FOR OTHER PLAYERS\nPRESS START";
            case EPlayerId.PLAYER1:
                PlayerTitleUIObjects[0].GetComponent<Image>().color = newColor;
                //if ( playersFoundCount < 4 )
                    //text = "WAITING FOR OTHER PLAYERS";
                if ( playersFoundCount > 3 )
                    text = "2v2";
                if( playersFoundCount == 1 )
                    text += "\nPRESS START";
                break;

            case EPlayerId.PLAYER2:
                PlayerTitleUIObjects[1].GetComponent<Image>().color = newColor;
                //if ( playersFoundCount < 4 )
                    //text = "WAITING FOR OTHER PLAYERS";
                if ( playersFoundCount > 3 )
                    text = "2v2";
                if ( playersFoundCount == 1 )
                    text += "\nPRESS START";
                break;

            case EPlayerId.PLAYER3:
                PlayerTitleUIObjects[2].GetComponent<Image>().color = newColor;
                text = "2v2";
                text += "\nWAITING FOR OTHER PLAYERS";
                //text += "\nPRESS START";
                break;

            case EPlayerId.PLAYER4:
                PlayerTitleUIObjects[3].GetComponent<Image>().color = newColor;
                text = "2v2";
                text += "\nWAITING FOR OTHER PLAYERS";
                //text += "\nPRESS START";
                break;
        }

        UI_CanvasTitle.transform.Find("Text").GetComponent<Text>().text = text;
    }

    public void MakePlayerReady(EPlayerId ePlayerId)
    {
        Color newColor = new Color(1f,1f,1f,1.0f);
        switch ( ePlayerId )
        {
            //.text = "WAITING FOR OTHER PLAYERS\nPRESS START";
            case EPlayerId.PLAYER1:
                PlayerTitleUIObjects[0].GetComponent<Image>().color = newColor;
                break;

            case EPlayerId.PLAYER2:
                PlayerTitleUIObjects[1].GetComponent<Image>().color = newColor;
                break;

            case EPlayerId.PLAYER3:
                PlayerTitleUIObjects[2].GetComponent<Image>().color = newColor;
                break;

            case EPlayerId.PLAYER4:
                PlayerTitleUIObjects[3].GetComponent<Image>().color = newColor;
                break;
        }
    }


    public void AnounceWinner(int teamId)
    {
        winningTeam = teamId;
        ChangeToGameFinishState();
    }

    public void OnGameFinished()
    {
        //Time.timeScale = 0.0f;

        UI_CanvasFinish.SetActive(true);
        UI_CanvasGameplay.SetActive(false);
        UI_CanvasTitleBackground.SetActive(true);

        Color newColor = new Color(1f,1f,1f,1.0f);

        Text textComp = UI_CanvasFinish.transform.Find("Text").GetComponent<Text>();

        if ( winningTeam == 0 )
        {
            PlayerFinishUIObjects[0].GetComponent<Image>().color = newColor;
            if (e_VersusMode == EVersusMode.TWO_TWO)
                PlayerFinishUIObjects[2].GetComponent<Image>().color = newColor;
            else
            {
                PlayerFinishUIObjects[2].SetActive(false);
                PlayerFinishUIObjects[3].SetActive(false);
            }
            textComp.text = "CONGRATULATIONS\nTEAM BLUE!!!";
        }
        else
        {
            PlayerFinishUIObjects[1].GetComponent<Image>().color = newColor;
            if ( e_VersusMode == EVersusMode.TWO_TWO )
                PlayerFinishUIObjects[3].GetComponent<Image>().color = newColor;
            else
            {
                PlayerFinishUIObjects[2].SetActive(false);
                PlayerFinishUIObjects[3].SetActive(false);
            }
            textComp.text = "CONGRATULATIONS\nTEAM RED!!!";
        }

        Invoke("RebootGame", timeUntilReboot);
    }

    public void RebootGame()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }
}
