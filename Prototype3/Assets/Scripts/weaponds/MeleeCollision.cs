﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCollision : MonoBehaviour {

    public MeleeHammer par;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
		if (other.tag == "Player" && other.gameObject != par.gameObject && par.isAnimating && par.team != other.GetComponent<PlayerInfo>().team)
        {
            other.GetComponent<PlayerInfo>().DealDamage(par.damage);
        }
    }
}
