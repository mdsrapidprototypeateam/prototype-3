﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public bool canShoot = true;
    public float rate;
    public float reloadRate;
    public float currentAmmo;
    public float maxAmmo;
    public int damage;
    protected bool isReloading;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void Fire(Transform cam)
    {
    }

    public virtual void Reload()
    {
    }

    public virtual void StopFire()
    {
    }

    public virtual void OnGet()
    {
    }

    public virtual void OnRelease()
    {
    }
}
