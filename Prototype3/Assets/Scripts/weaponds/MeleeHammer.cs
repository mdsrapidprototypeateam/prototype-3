﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHammer :  Weapon {

	public GameObject projectilePrefab;
    public GameObject meleeObject;
    public Camera cam;
    public bool isAnimating;
	public int team;

    public string meleeSound = "event:/MeleeSwing";

    // Use this for initialization
    void Start () {
		team = GetComponent<PlayerInfo> ().team;
	}

	// Update is called once per frame
	void Update () {
        if (isAnimating)
        {
            meleeObject.transform.Rotate(10, 0, 0, Space.Self);
            if (meleeObject.transform.rotation.eulerAngles.x >= 90)
            {
                isAnimating = false;
                meleeObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
        }
        
	}

	public override void Fire(Transform c)
	{
		if (!isReloading)
		{
			if (canShoot)
			{
				base.Fire(c);

                isAnimating = true;
                FMODUnity.RuntimeManager.PlayOneShot(meleeSound);
				StartCoroutine(FireRate());

			}
		}
	}

    public override void OnGet()
    {
        base.OnGet();
        meleeObject.SetActive(true);
    }

    public override void OnRelease()
    {
        base.OnGet();
        meleeObject.SetActive(false);
    }

    public override void Reload()
	{
		base.Reload();
		StartCoroutine(ReloadTime());
	}

	IEnumerator FireRate()
	{
		canShoot = false;
		yield return new WaitForSeconds(rate);
		canShoot = true;
	}

	IEnumerator ReloadTime()
	{
		isReloading = true;
		yield return new WaitForSeconds(reloadRate);
		currentAmmo = maxAmmo;
		isReloading = false;
	}

}

