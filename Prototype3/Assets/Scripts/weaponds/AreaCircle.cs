﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCircle : MonoBehaviour {

    public int damage;
    public float duration;
    public float tickRate;
    public int team;
    public List<PlayerInfo> list = new List<PlayerInfo>();

	// Use this for initialization
	void Start () {
        InvokeRepeating("DealDamage", 0, tickRate);
        Destroy(gameObject, duration);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void DealDamage()
    {
        foreach(PlayerInfo p in list)
        {
            if (team != p.team)
            {
                p.DealDamage(damage);
            }
        }
    }
}
