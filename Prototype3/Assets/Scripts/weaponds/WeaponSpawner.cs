﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawner : MonoBehaviour {


	public GameObject[] weaponds;

	//  private MyGameManager gameManager;

	public bool spawnTimerActive;// variable that represents the need for spawning a powerup
	private float innerTimer;

	public float spawnPUPTimer;
	public Vector3 spawnPos;




	// Use this for initialization
	void Start()
	{
		spawnTimerActive = true;

	}

	// Update is called once per frame
	void Update()
	{

		if (spawnTimerActive)
		{
			SpawnTimer();
		}


	}


	//function responsable for spawing a weapond after a certain time when ever it is necessary
	void SpawnTimer()
	{
		innerTimer += Time.deltaTime;

		if (innerTimer > spawnPUPTimer)
		{
			RandomWeapond ();
			spawnTimerActive = false;
			innerTimer = 0;
		}

	}


	//responsable for picking a random weapond up to spawn
	// this function can be afjusted in order to have some weaponds have more possibility of spawn then others
	private void RandomWeapond()
	{
		float ranNum = Random.Range(0, 100);


		//Spawn weapond one
		if(ranNum<=35)
		{
			GameObject pUp = Instantiate(weaponds[0], transform.position+spawnPos, Quaternion.identity, this.transform);
			pUp.GetComponent<WeapondPickUp> ().spawner = this.gameObject;

		}
		//Spawn weapond two
		else if(ranNum>35 && ranNum<=65)
		{
			GameObject pUp = Instantiate(weaponds[1], transform.position+spawnPos, Quaternion.identity, this.transform);
			pUp.GetComponent<WeapondPickUp> ().spawner = this.gameObject;
		}
		//Spawn weapond three
		else if (ranNum > 65 && ranNum <= 100)
		{
			GameObject pUp = Instantiate(weaponds[2], transform.position+spawnPos, Quaternion.identity, this.transform);
			pUp.GetComponent<WeapondPickUp> ().spawner = this.gameObject;
		}
		else
		{
			//something went wrong
		}

	}





	void OnTriggerEnter(Collider col)
	{

	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.CompareTag("weapond"))
		{
			spawnTimerActive = true;
		}

	}
}
