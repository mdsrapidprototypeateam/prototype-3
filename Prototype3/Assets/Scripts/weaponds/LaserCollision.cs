﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCollision : MonoBehaviour {

    private ParticleSystem PSystem;
    private List<ParticleCollisionEvent> CollisionEvents;
    public LaserBeam laserScript;

    // Use this for initialization
    void Start () {
        PSystem = GetComponent<ParticleSystem>();
        CollisionEvents = new List<ParticleCollisionEvent>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnParticleCollision(GameObject other)
    {
        int eventCount = PSystem.GetCollisionEvents(other, CollisionEvents);

        for (int i = 0; i < eventCount; i++)
        {
			if(other.tag == "Player" && other != laserScript.gameObject && laserScript.team != other.GetComponent<PlayerInfo>().team)
            {
                laserScript.SetTarget(other);
                other.GetComponent<PlayerInfo>().DealDamage(laserScript.damage);
            }
            //TODO: Do your collision stuff here. 
            // You can access the CollisionEvent[i] to obtaion point of intersection, normals that kind of thing
            // You can simply use "other" GameObject to access it's rigidbody to apply force, or check if it implements a class that takes damage or whatever
        }
    }
}
