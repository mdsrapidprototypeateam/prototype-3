﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeapondPickUp : MonoBehaviour {

	public float deathCD;
	private float innerTimer;
	public int type;
	public float rotSpeed;
	public Vector3 spinVector;

	public GameObject spawner;


	private bool active;

	// Use this for initialization
	void Start()
	{
		active = false;


	}

	// Update is called once per frame
	void Update()
	{
		DeathTimer();
		DoABarrelRoll();
	}

	void DeathTimer()
	{
		innerTimer += Time.deltaTime;

		if (innerTimer > deathCD)
		{
			PosDeath ();

		}
	}



	//just makes the object spin for visual effects
	void DoABarrelRoll()
	{
		transform.Rotate(spinVector * Time.deltaTime * rotSpeed);

	}




	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Player"))
		{
            col.GetComponent<PlayerMovement>().currentWeapon.OnRelease();
            switch (type)
			{
			case 1:
                
                col.GetComponent<PlayerMovement> ().currentWeapon = col.GetComponent<GrenadeLauncher>();
				break;
			case 2:
				col.GetComponent<PlayerMovement> ().currentWeapon =  col.GetComponent<LaserBeam>();
				break;
			case 3:
				col.GetComponent<PlayerMovement> ().currentWeapon =  col.GetComponent<MeleeHammer>();
				break;
			}
            col.GetComponent<PlayerMovement>().currentWeapon.OnGet();
            PosDeath ();
		}

	}



	void PosDeath()
	{
		spawner.GetComponent<WeaponSpawner> ().spawnTimerActive = true;
		Destroy (this.gameObject);
	}
}
