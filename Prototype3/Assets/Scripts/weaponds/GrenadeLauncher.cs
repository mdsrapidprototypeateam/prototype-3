﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncher : Weapon {

    public GameObject projectilePrefab;
    public List<GameObject> bombAreas = new List<GameObject>();
    public float duration;
    public float tickRate;

    public string nadeSound = "event:/Grenade";

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public override void Fire(Transform c)
    {
        if (!isReloading)
        {
            if (canShoot)
            {
                base.Fire(c);
                FMODUnity.RuntimeManager.PlayOneShot(nadeSound);
                GameObject o = Instantiate(projectilePrefab, transform.position + transform.rotation * new Vector3(2f, 0, 2f), Quaternion.identity, null);
                o.GetComponent<Rigidbody>().AddForce(c.rotation * new Vector3(-3f, 10f, 30f), ForceMode.Impulse);
                o.GetComponent<GrenadeProjectile>().launcher = this;
                StartCoroutine(FireRate());
                currentAmmo--;
                if (currentAmmo == 0)
                {
                    Reload();
                }
            }
        }
    }

    public override void Reload()
    {
        base.Reload();
        StartCoroutine(ReloadTime());
    }

    IEnumerator FireRate()
    {
        canShoot = false;
        yield return new WaitForSeconds(rate);
        canShoot = true;
    }

    IEnumerator ReloadTime()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadRate);
        currentAmmo = maxAmmo;
        isReloading = false;
    }
}
