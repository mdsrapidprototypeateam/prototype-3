﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam :  Weapon {

	public ParticleSystem laserParticles;
    public Transform target = null;
    public float maxRange;
    public Camera cam;
	public int team;

    public string laserSound = "event:/Laser";
    FMOD.Studio.EventInstance laserEvent;


    // Use this for initialization
    void Start () {
		team = GetComponent<PlayerInfo> ().team;
        laserEvent = FMODUnity.RuntimeManager.CreateInstance(laserSound);

    }

	// Update is called once per frame
	void Update () {
        if (target)
        {
            RaycastHit hit;

            if ((target.transform.position - transform.position).magnitude >= maxRange){
                target = null;
            }

            else if (cam.WorldToViewportPoint(target.position).x < 0 || cam.WorldToViewportPoint(target.position).x > 1 || cam.WorldToViewportPoint(target.position).y < 0 || cam.WorldToViewportPoint(target.position).y > 1)
            {
                target = null;
            }

            
            else if(Physics.Linecast(transform.position, target.position, out hit))
            {
                if (hit.transform != target.transform)
                {
                    target = null;
                }
            }
        }
	}

	public override void Fire(Transform c)
	{
		if (!isReloading)
		{
			base.Fire(c);
            if (!laserParticles.gameObject.activeInHierarchy)
            {
                laserParticles.gameObject.SetActive(true);
                laserEvent.start();
                
            }

            if (!target)
            {
                laserParticles.transform.rotation = c.rotation;
                
            } else
            {
                laserParticles.transform.LookAt(target);
                
            }

            if (canShoot)
            {
                currentAmmo--;
                StartCoroutine(FireRate());
                if (currentAmmo == 0)
                {
                    StopFire();
                    Reload();
                }
            }

		}
	}

    public override void StopFire()
    {
        base.StopFire();
        laserParticles.gameObject.SetActive(false);
        target = null;
        laserEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    public override void Reload()
	{
		base.Reload();
		StartCoroutine(ReloadTime());
	}

	IEnumerator FireRate()
	{
        canShoot = false;
		yield return new WaitForSeconds(rate);
		canShoot = true;
	}

	IEnumerator ReloadTime()
	{
		isReloading = true;
		yield return new WaitForSeconds(reloadRate);
		currentAmmo = maxAmmo;
		isReloading = false;
	}

    public override void OnRelease()
    {
        base.OnRelease();
        laserEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    public void SetTarget(GameObject o)
    {
        if (!target)
        {
            target = o.transform;
        }
    }
}
