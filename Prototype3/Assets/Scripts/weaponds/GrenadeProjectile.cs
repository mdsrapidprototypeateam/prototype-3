﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeProjectile : MonoBehaviour {

    public GameObject areaPrefab;
    public GrenadeLauncher launcher;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        
        GameObject o = Instantiate(areaPrefab, transform.position - new Vector3(0, 0.6f), Quaternion.identity);
        o.transform.forward = collision.contacts[0].normal;
        o.GetComponent<AreaCircle>().damage = launcher.damage;
        o.GetComponent<AreaCircle>().duration = launcher.duration;
        o.GetComponent<AreaCircle>().tickRate = launcher.tickRate;
        o.GetComponent<AreaCircle>().team = launcher.GetComponent<PlayerInfo>().team;
        launcher.bombAreas.Add(o);
        if (launcher.bombAreas.Count >= 3)
        {
            Destroy(launcher.bombAreas[0]);
            launcher.bombAreas.RemoveAt(0);
        }
        Destroy(gameObject);
    }
}
