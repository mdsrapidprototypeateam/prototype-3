﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputBase : MonoBehaviour
{
    public MyGameManager gameManager;

    [Header("Input")]
    public EPlayerId ePlayerId = EPlayerId.PLAYER1;
    public PlayerId playerIdComponent;

    public string horizontalMovAxisName = "MovHorizontal";
    public string verticalMovAxisName = "MovVertical";
    public string horizontalAimAxisName = "AimHorizontal";
    public string verticalAimAxisName = "AimVertical";
    public string fireButtonName = "Fire1";
    public string jumpButtonName = "Jump";
    public string startButtonName = "Submit";
    public string reloadButtonName = "Reload";

    public float m_fMovHorizontalInput = 0.0f;
    public float m_fMovVerticalInput = 0.0f;
    public float m_fAimHorizontalInput = 0.0f;
    public float m_fAimVerticalInput = 0.0f;

    public bool m_bIsFireHold = false;
    public bool m_bIsJumpHold = false;
    public bool m_bIsStartHold = false;
    public bool m_bIsReloadHold = false;

    public float m_FireTriggerInput = 0.0f;

    [HideInInspector]
    public string inputSuffix = "";

    public virtual void Start()
    {
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        playerIdComponent = GetComponent<PlayerId>();
        ePlayerId = playerIdComponent.playerId;

        AddSufixToInputNames();
    }

    public virtual void Update()
    {
        GetPlayerInput(Time.deltaTime);
    }

    public virtual void FixedUpdate()
    {
        GetPlayerPhysicsInput(Time.deltaTime);
    }

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public virtual void GetPlayerPhysicsInput(float deltaTime) { }

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public virtual void GetPlayerInput(float deltaTime) { }

    public void AddSufixToInputNames()
    {
        inputSuffix = "_";
        switch ( ePlayerId )
        {
            case EPlayerId.PLAYER1:
                inputSuffix += 1;
                break;
            case EPlayerId.PLAYER2:
                inputSuffix += 2;
                break;
            case EPlayerId.PLAYER3:
                inputSuffix += 3;
                break;
            case EPlayerId.PLAYER4:
                inputSuffix += 4;
                break;
        }

        horizontalMovAxisName += inputSuffix;
        verticalMovAxisName += inputSuffix;
        horizontalAimAxisName += inputSuffix;
        verticalAimAxisName += inputSuffix;
        fireButtonName += inputSuffix;
        jumpButtonName += inputSuffix;
        startButtonName += inputSuffix;
        reloadButtonName += inputSuffix;
    }
}
