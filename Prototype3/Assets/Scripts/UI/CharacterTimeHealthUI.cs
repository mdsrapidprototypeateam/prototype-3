﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterTimeHealthUI : MonoBehaviour
{
    public Image progressbar;
    public PlayerInfo playerInfo;

    // Use this for initialization
    void Start ()
    {
        progressbar = GetComponent<Image>();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (playerInfo)
        {
            progressbar.fillAmount = playerInfo.currentTime * 0.01f;
        }
	}
}
