﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EProgressType { FINAL, CAP, CONTESTING, NONE };

public class ProgressManagerWidget : MonoBehaviour
{
    public CapPointManager capPoint;

    public EProgressType progressType = EProgressType.NONE;

    [Header("Global Progress")]
    public Image progressbarTeam1;
    public Image progressbarTeam2;
    public Text progressTextTeam1;
    public Text progressTextTeam2;

    [Range(0, 100)]
    public int progressValueTeam1 = 0;
    [Range(0, 100)]
    public int progressValueTeam2 = 0;

    [Header("Capturing Progress")]
    public Image ownerBadgeTeam1;
    public Image ownerBadgeTeam2;

    void Start ()
    {
        capPoint = GameObject.FindObjectOfType<CapPointManager>();

        FindProgressUIComponents();

    }

    void Update ()
    {
        TrackTeamsProgress();   //get progress values from capPointManager
        UpdateUI();             //update UI with last values
    }


    private void FindProgressUIComponents()
    {
        Image[] imageComponents = gameObject.GetComponentsInChildren<Image>();
        Text[] textComponents = gameObject.GetComponentsInChildren<Text>();
        int size = imageComponents.Length;
        for ( int i = 0; i < size; i++ )
        {
            if ( imageComponents[i].tag.Equals("Team1"))
            {
                if( !imageComponents[i].name.Contains("Owner") )
                    progressbarTeam1 = imageComponents[i];
                else
                    ownerBadgeTeam1 = imageComponents[i];
            }
            else if ( imageComponents[i].tag.Equals("Team2") )
            {
                if ( !imageComponents[i].name.Contains("Owner") )
                    progressbarTeam2 = imageComponents[i];
                else
                    ownerBadgeTeam2 = imageComponents[i];
            }
        }

        if( progressType == EProgressType.FINAL)
        {
            size = textComponents.Length;
            for ( int i = 0; i < size; i++ )
            {
                if ( textComponents[i].tag.Equals("Team1") )
                {
                    progressTextTeam1 = textComponents[i];
                }
                else if ( textComponents[i].tag.Equals("Team2") )
                {
                    progressTextTeam2 = textComponents[i];
                }
            }
        }

        if ( progressType == EProgressType.CAP || progressType == EProgressType.CONTESTING )
        {
            size = textComponents.Length;
            for ( int i = 0; i < size; i++ )
            {
                if ( textComponents[i].tag.Contains("Team") )
                {
                    progressTextTeam1 = textComponents[i];
                }
            }
        }
    }


    /// <summary>
    /// get progress values from capPointManager
    /// </summary>
    private void TrackTeamsProgress()
    {
        if ( progressType == EProgressType.FINAL )
        {
            progressValueTeam1 = capPoint.team0FinalProgress;
            progressValueTeam2 = capPoint.team1FinalProgress;
        }
        else if ( progressType == EProgressType.CAP )
        {
            progressValueTeam1 = (int) capPoint.outTeam0CapProgress;
            progressValueTeam2 = (int) capPoint.outTeam1CapProgress;
        }
    }

    /// <summary>
    /// Update UI widgets: progress bar and progress text
    /// </summary>
    void UpdateUI()
    {
        if ( progressType == EProgressType.FINAL && MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED)
        {
            progressTextTeam1.text = FormatIntProgressString(progressValueTeam1);
            progressTextTeam2.text = FormatIntProgressString(progressValueTeam2);

            if( progressValueTeam1 == 100 )
            {
                MyGameManager.GetInstance().AnounceWinner(0);
            }
            else if ( progressValueTeam2 == 100 )
            {
                MyGameManager.GetInstance().AnounceWinner(1);
            }
        }

        if ( progressType == EProgressType.CAP && MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED )
        {
            string ownerTeam = "";

            if (capPoint.owner == CapPointManager.CaptState.TEAM1 )
            {
                ownerBadgeTeam1.fillAmount = 1.0f;
                ownerBadgeTeam2.fillAmount = 0f;
                ownerTeam = "B";
            }
            else if ( capPoint.owner == CapPointManager.CaptState.TEAM2 )
            {
                ownerBadgeTeam2.fillAmount = 1.0f;
                ownerBadgeTeam1.fillAmount = 0f;
                ownerTeam = "R";
            }
            else
            {
                ownerBadgeTeam1.fillAmount = 0f;
                ownerBadgeTeam2.fillAmount = 0f;
            }

            progressTextTeam1.text = ownerTeam;

        }

        if ( progressType != EProgressType.CONTESTING && MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED )
        {

            progressbarTeam1.fillAmount = progressValueTeam1 * 0.01f;
            progressbarTeam2.fillAmount = progressValueTeam2 * 0.01f;
        }
        else if ( progressType == EProgressType.CONTESTING && MyGameManager.GetInstance().e_State == EGameState.STATE_GAME_STARTED )
        {
            progressTextTeam1.enabled = capPoint.contexted;
        }
    }

    /// <summary>
    /// Write integer as a formated string for progress bar UI
    /// </summary>
    /// <param name="value">int | the progress value</param>
    /// <returns>string | the formated string</returns>
    string FormatIntProgressString(int value)
    {
        string progress = "";
        string prefix = "";
        string sufix = "%";

        if ( value < 10 )
            prefix = "0";
        else if ( value >= 10 && value < 100)
            prefix = "";

        progress = prefix + value.ToString() + sufix;

        return progress;
    }
}
