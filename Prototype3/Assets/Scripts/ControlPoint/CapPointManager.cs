﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapPointManager : MonoBehaviour 
{
	public GameObject[] points;
	public float movSpeed;
	public float rotSpeed;
	public float proximityError;//the amount of space around the point before it is considered close enought to move to the next point
	public int team0Players;
	public int team1Players;
	public int team0FinalProgress;
	private float team0CapProgress;
	public float outTeam0CapProgress;
	public int team1FinalProgress;
	private float team1CapProgress;
	public float outTeam1CapProgress;
	public bool contexted;


	public enum CaptState
	{
		NONE,
		TEAM1,
		TEAM2
	}

	public CaptState conquering;
	public CaptState owner;
	public float decayTimer;//the amount of time before the capture progress starts to decay on the point
	public float captureSpeed;
	public float decaySpeed;
	public float conqueringTick;
	public float delayTillCap;//the amount of time until the point is capturable in the begining of the game


	private float innerConqueringTick;
	private float innerDecayTimer;
	public List<GameObject> closePlayers;
	private Vector3 prePos;
	private int nextIntPos;

    [FMODUnity.EventRef]
    public string captureSound = "event:/Capturing";
    FMOD.Studio.EventInstance captureEvent;
    public string contestingSound = "event:/Contesting";
    FMOD.Studio.EventInstance contestingEvent;
    public string spiderSound = "event:/SpiderWalk";
    FMOD.Studio.EventInstance spiderEvent;

    bool captureIsPlaying;
    bool contestIsPlaying;
    bool justCaptured;

    // Use this for initialization
    void Start () 
	{
		prePos =  points[0].transform.position;
		innerConqueringTick = conqueringTick;
		innerDecayTimer = -10;
		contexted = false;
        captureEvent = FMODUnity.RuntimeManager.CreateInstance(captureSound);
        contestingEvent = FMODUnity.RuntimeManager.CreateInstance(contestingSound);
        spiderEvent = FMODUnity.RuntimeManager.CreateInstance(spiderSound);
        spiderEvent.start();
    }
	
	// Update is called once per frame
	void Update () 
	{
		BaseMovement ();

        ConqueringState();
        ConqueringProgress();
        DecayCheck();
        ConqueredPoints(); 


    }


	//Movement function
	//follows a set of points set in the points array on the inspector
	//uses both the movSpeed and rotSpeed from the inspector
	//besides moving the object to the target also rotates to face the target
	void BaseMovement()
	{
		Vector3 nextPos;
		Vector3 newDir;
		//reset path
		if (nextIntPos == points.Length-1)
		{
			nextIntPos = 0;
		}
			
		if (Vector3.Distance(transform.position,prePos) >= proximityError) 
		{
			nextPos = prePos;
		} 
		else
		{
			nextIntPos += 1;
			nextPos = points[nextIntPos].transform.position;
			prePos=points[nextIntPos].transform.position;
		}
			
		transform.position = Vector3.MoveTowards(transform.position, nextPos, movSpeed * Time.deltaTime);

		// make the object look at the target it is moving towards next
		newDir = new Vector3 (nextPos.x, transform.position.y, nextPos.z) - transform.position;
		Debug.DrawRay (transform.position, newDir, Color.red);
		Quaternion finnishLook = Quaternion.LookRotation (newDir);
		finnishLook *= Quaternion.Euler (0, 90, 0);// becouse the object is currently rotated 90 degrees
		transform.rotation = Quaternion.Slerp(transform.rotation,finnishLook,rotSpeed*Time.deltaTime);

	}












	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.CompareTag("Player"))
		{
			
			closePlayers.Add (col.gameObject);
			if (col.GetComponent<PlayerInfo> ().team == 0) 
			{
				team0Players += 1;
			}
			else
			{
				team1Players += 1;
			}
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.CompareTag("Player"))
		{

			closePlayers.Remove (col.gameObject);
			if (col.GetComponent<PlayerInfo> ().team == 0) 
			{
				team0Players -= 1;
			}
			else
			{
				team1Players -= 1;
			}
		}
	}




	void ConqueringState()
	{
		outTeam0CapProgress = Mathf.Round(team0CapProgress);
		outTeam1CapProgress = Mathf.Round(team1CapProgress);



		if (team0CapProgress >= 100)
		{
			team0CapProgress = 100;
			owner = CaptState.TEAM1;
			team0CapProgress = 0;
			innerDecayTimer = -10;
            captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }

		if (team1CapProgress >= 100)
		{
			team1CapProgress = 100;
			owner = CaptState.TEAM2;
			team1CapProgress = 0;
			innerDecayTimer = -10;
            captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }

		if (team1CapProgress == 0 && team1CapProgress == 0) 
		{
			conquering = CaptState.NONE;
            //captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
		else if (team0CapProgress != 0 && team1CapProgress == 0) 
		{
			conquering = CaptState.TEAM1;
            
		}
		else if (team1CapProgress != 0 && team0CapProgress == 0) 
		{
			conquering = CaptState.TEAM2;
            captureEvent.start();
            
        }


	}




	//Function responsable for checking the number players close to the object and acting acordingly
	//if both teams are on the point it is being contest and therefor nothing happens
	//if one teams manages to get time alone in the point their conquering progress will increase
	void ConqueringProgress()
	{
		if (team0Players != 0 && team1Players != 0)
		{
			//point being conquested nothing happens in terms of capturing progress
			contexted=true;
			innerDecayTimer = decayTimer;
            if (!contestIsPlaying)
            {
                captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                contestingEvent.start();
                captureIsPlaying = false;
                contestIsPlaying = true;
            }
            

        }
		else if(team0Players!=0)
		{
			contexted = false;
			if (owner == CaptState.NONE || owner == CaptState.TEAM2)
			{
				if (conquering == CaptState.NONE || conquering == CaptState.TEAM1)
				{
                    //progress for team 0
					team0CapProgress+=captureSpeed*team0Players;
					innerDecayTimer = decayTimer;
                    if (!captureIsPlaying)
                    {
                        contestingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                        captureEvent.start();
                        captureIsPlaying = true;
                        contestIsPlaying = false;
                    }
                }
				//nothing becouse the other team was conquering the point and this team must wait for their progress to decay
			
			}
            
            //else nothing becouse the team already owns the point

        }
		else if(team1Players!=0)
		{
			contexted = false;
			if (owner == CaptState.NONE || owner == CaptState.TEAM1)
			{
				if (conquering == CaptState.NONE || conquering == CaptState.TEAM2) 
				{
					//progress for team 1
					team1CapProgress+=captureSpeed*team1Players;
					innerDecayTimer = decayTimer;
                    if (!captureIsPlaying)
                    {
                        contestingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                        captureEvent.start();
                        captureIsPlaying = true;
                        contestIsPlaying = false;
                    }
                }
				//nothing becouse the other team was conquering the point and this team must wait for their progress to decay
			}

            if (captureIsPlaying)
            {
                captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                captureIsPlaying = false;
            }
            //else nothing becouse the team already owns the point
        }

        else
        {
            if (contestIsPlaying)
            {
                contestingEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                contestIsPlaying = false;
            } else if (captureIsPlaying)
            {
                captureEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                captureIsPlaying = false;
            }

        }

	}

	//function that is responsable for checking if the decay timer has passed in order to reset conquest progress
	void DecayCheck()
	{

		if (innerDecayTimer <= 0 && innerDecayTimer>-10)
		{
            //start decaying capture
            //team 0
            if (team0CapProgress > 0)
			{
				team0CapProgress -= decaySpeed;
			}
			else if (team0CapProgress < 0) 
			{
				team0CapProgress = 0;
			}


			//team 1
			if (team1CapProgress > 0)
			{
				team1CapProgress -= decaySpeed;
			} 
			else if (team1CapProgress < 0) 
			{
				team1CapProgress = 0;
			}


		} 
		else if (innerDecayTimer < -10) 
		{

			//do nothing
		}
		else 
		{
			innerDecayTimer -= Time.deltaTime;
		}
	}



	// function responsable for incrementaly increasing the conquering progress of the team that currently owns the point
	// it uses the conqueringtick variable set on the inpector as the set amount of each tick
	void ConqueredPoints()
	{
		if (innerConqueringTick <= 0)
		{
			if (owner == CaptState.TEAM1) 
			{
				team0FinalProgress += 1;
			} 
			else if (owner == CaptState.TEAM2) 
			{
				team1FinalProgress += 1;
			}

			innerConqueringTick = conqueringTick; 
		}
		else 
		{
			innerConqueringTick -= Time.deltaTime;
		}

	}


}
